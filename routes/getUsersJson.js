const {Router} = require('express')
const router = Router()
const mongoose = require('../mgModels/connection')
const Users = require('../mgModels/users') 

router.get('/', (req, res) =>
Users.find()
     .exec()
     .then(usersList => res.json(usersList))
);

module.exports = router