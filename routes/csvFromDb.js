const {Router} = require('express')
const router = Router()
const { Parser } = require('json2csv')
const mongoose = require('../mgModels/connection')
const fs = require('fs')
const Users = require('../mgModels/users')
const fields = ['UserName', 'FirstName', 'LastName', 'Age']
const opts = { fields }

router.get('/', (req, res) =>
Users.find()
.exec()
.then(usersList => {
    try {
        const parser = new Parser(opts)
        const csv = parser.parse(usersList)
        if (!fs.existsSync('./public')){
            fs.mkdirSync('./public');
        }
        const path = './public/file'+ Date.now() +'.csv'
        fs.writeFile(path, csv, function(err, data) {
            if (err) {throw err}
            else{ 
                res.download(path)
                }
            }); 
      } catch (err) {
        console.error(err)
      }
    })    
);

module.exports = router