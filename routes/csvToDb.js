const {Router} = require('express')
const router = Router()
const csv = require('csvtojson')
const multer  = require('multer')
const upload = multer({ dest: 'tmp/csv/' })
const mongoose = require('../mgModels/connection')

  const Schema = mongoose.Schema
  const userSchema = new Schema({
    UserName: String,
    FirstName: String,
    LastName: String,
    Age: String,
   })
  
 const User = mongoose.model('User', userSchema)

router.post('/', upload.single('fileCsv'),  function (req, res) {
    csv()
    .fromFile(req.file.path)
    .then((jsonObj)=>{
        User.collection.insertMany(jsonObj, function(err,r) {
            if(err)  throw err
            else {
                res.sendStatus(200)
            }
        })
    }) 
  })

module.exports = router