const express = require('express')
const app = express()
const routeCsvToDb = require('./routes/csvToDb')
const routegetUsersJson = require('./routes/getUsersJson')
const routeCsvFromDb = require('./routes/csvFromDb')
const PORT = 3000 || process.env.PORT
 
app.use('/api/uploadCsvToDb', routeCsvToDb)
app.use('/api/getUsersJson', routegetUsersJson)
app.use('/api/downloadCsvFromDb', routeCsvFromDb)
 
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`)
})